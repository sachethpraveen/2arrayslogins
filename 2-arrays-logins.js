const people = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

//Problem 1
function findGender(array, gender) {
  return array.reduce((accumulator, current) => {
    if (current.gender === gender) {
      accumulator.push(current);
    }
    return accumulator;
  }, []);
}

//Problem 2
function ipAddressComponents(array) {
  return array.map((element) => {
    element.ip_address = element.ip_address.split(".");
    return element;
  });
}

//Problem 3
function ipAddressComponentSum(array, index) {
  return array.reduce((accumulator, current) => {
    let components = current.ip_address.split(".");
    accumulator += parseInt(components[index - 1]);
    return accumulator;
  }, 0);
}

//Problem 4
function fullName(array) {
  return array.map((element) => {
    element["full_name"] = element.first_name + " " + element.last_name;
    return element;
  });
}

//Problem 5
function filterEmail(array, extension) {
  return array.filter((element) => {
    let emailComponents = element.email.split(".");
    if (emailComponents[emailComponents.length - 1] === extension) {
      return element;
    }
  });
}

//Problem 6
function countEmailExtensions(array, extensions) {
  return array.reduce((accumulator, current) => {
    let emailComponents = current.email.split(".");
    if (extensions.includes(emailComponents[emailComponents.length - 1])) {
      if (
        accumulator.hasOwnProperty(emailComponents[emailComponents.length - 1])
      ) {
        accumulator[emailComponents[emailComponents.length - 1]]++;
      } else {
        accumulator[emailComponents[emailComponents.length - 1]] = 1;
      }
    }
    return accumulator;
  }, {});
}

//Problem 7
function sortInDescendingOrderFirstName(array) {
  return array.sort((a, b) => {
    return b.first_name.localeCompare(a.first_name);
  });
}
